#!/usr/bin/env node
let { SIGKILL } = require('constants');
let yargs = require('yargs');
let net = require("net");
let args = yargs.number('delay').default('delay', 0).parse();
let print = (...args) => console.log(...args);
let [host, ports] = args._;
let { delay } = args;

if (!host || host.trim().length == 0) return print(`You must enter hostname`);
if (!ports || String(ports).trim().length == 0) return print(`You must enter ports`)

let portsArray = typeof ports == 'number' ? [ports] : ports.split(',');

function checkPort(host, port) {
    return new Promise((resolve, reject) => {
        net.connect({
            host: host, port: port,
            timeout: 5000
        })
        .once('connect', _ => resolve(true))
        .once('error', _ => reject(_))
    });
}

function checkPorts() {
    print(`Checking ports ${ portsArray.join(', ') } on host ${ host }`);
    let promises = portsArray.map( port => {
        return checkPort(host, port)
            .then(_ => print(` - Port ${ port } is OPEN`))
            .catch( _ => print(` - Port ${ port } is CLOSED`))
    });

    Promise.all(promises).then( _ => {
        print(`All ports checked.`)
        if ( delay ) {
            setTimeout(checkPorts, delay);
        } else {
            process.exit(SIGKILL);
        }
    })
}

checkPorts();